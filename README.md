# Level 1 of ICL Handout
### See branch `Level2` for the Level 2 of the handout

### To run the project execute the following commands:

```
cd src
javacc Parser.jj
javac Main.java
java Main
# Now just write the code!
```

### Changes from the specification
#### Program end

In order to make complex programs easier to read, we removed the requirement of single line programs. Two new lines (`\n\n`) or `;;` now mark the end of the code.
