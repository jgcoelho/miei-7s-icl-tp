package compiler;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CodeBlock {
	private LinkedList<String> instructionList;
	private LinkedList<Frame> frameList;
	private LinkedList<Frame> stack;

	public CodeBlock() {
		instructionList = new LinkedList<>();
		frameList = new LinkedList<>();
		stack = new LinkedList<>();
	}
	
	public void addInstruction(String instruction) {
		instructionList.add(instruction);
	}
	
	public List<Frame> getFrameList() {
		return frameList;
	}
	
	public Iterator<Frame> getStackIterator() {
		return stack.descendingIterator();
	}

	public Frame beginScope() {
		Frame lastFrame = stack.peekLast();
		String ancestorType = null;
		if (lastFrame != null)
			ancestorType = lastFrame.getType();

		Frame frame = new Frame(frameList.size(), ancestorType);
		frameList.add(frame);
		stack.add(frame);
		instructionList.add("new " + frame.getType());
		instructionList.add("dup");
		instructionList.add("invokespecial " + frame.getType() + "/<init>()V");

		if (frame.getAncestorType() != null) {
			instructionList.add("dup");
			instructionList.add("aload 3");
			instructionList.add("putfield " + frame.getType() + "/sl L" + frame.getAncestorType() + ";");
		}

		instructionList.add("astore 3");
		return frame;
	}
	
	public void endScope() {
		Frame frame = stack.getLast();

		if (frame.getAncestorType() != null) {
			instructionList.add("aload 3");
			instructionList.add("getfield " + frame.getType() + "/sl L" + frame.getAncestorType() + ";");
			instructionList.add("astore 3");
		}

		stack.pollLast();
	}
	
	public String genAssembly(String className) {
		String output =
		".class public " + className + "\n" + 
		".super java/lang/Object\n" + 
		".method public <init>()V\n" + 
		"   aload_0\n" + 
		"   invokenonvirtual java/lang/Object/<init>()V\n" + 
		"   return\n" + 
		".end method\n" + 
		".method public static main([Ljava/lang/String;)V\n" + 
		"        .limit locals 10\n" + 
		"        .limit stack 256\n" + 
		"        getstatic java/lang/System/out Ljava/io/PrintStream;\n" +
		"\n";
		
		for(String instruction: this.instructionList) {
			output += "        " + instruction + "\n";
		}

		output += "\n" +
		"        invokestatic java/lang/String/valueOf(I)Ljava/lang/String;\n" + 
		"        invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V\n" + 
		"        return\n" + 
		".end method";
		
		return output;
	}
}
