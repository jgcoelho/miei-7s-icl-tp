package compiler;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Frame {
	private int id;
	private Map<String, FrameVar> identifiers;
	private int counter;
	private String ancestorType;
	
	public Frame(int id, String ancestorType) {
		this.id = id;
		identifiers = new HashMap<>();
		this.counter = 0;
		this.ancestorType = ancestorType;
	}
	
	public int getId() {
		return id;
	}
	
	public String getAncestorType() {
		return ancestorType;
	}
	
	public FrameVar getIdentififier(String id) {
		return this.identifiers.get(id);
	}

	public void alloc(String identifier) {
		FrameVar newVar = new FrameVar(counter++);
		FrameVar previousVar = identifiers.putIfAbsent(identifier, newVar);
		if (previousVar != null)
			throw new RuntimeException("Variable " + identifier + " already allocated");
	}
	
	public void initialize(String identifier) {
		getIdentififier(identifier).setInitialized();
	}
	
	public String getType() {
		return "Frame_" + this.getId();
	}

	public String genAssembly(String className) {
		String output = ".class " + this.getType() + "\n";
		output += ".super java/lang/Object\n";

		if (ancestorType != null)
			output += ".field public sl L" + ancestorType + ";\n";
		
		for(Entry<String, FrameVar> e : identifiers.entrySet()) {
			output += ".field public x_" + e.getValue().getVarNumber() + " I\n";
		}
		
		output += ".method public <init>()V\n"
				+ "    aload_0\n"
				+ "    invokenonvirtual java/lang/Object/<init>()V\n"
				+ "    return\n"
				+ ".end method\n";

		return output;
	}
}
