package compiler;

public class FrameVar {
	private int varNumber;
	private boolean initialized;
	
	public FrameVar(int varNumber) {
		this.varNumber = varNumber;
		this.initialized = false;
	}

	public int getVarNumber() {
		return varNumber;
	}

	public boolean isInitialized() {
		return initialized;
	}

	protected void setInitialized() {
		this.initialized = true;
	}
}
