package values;

public class VMCell implements IValue {
	private IValue v;
	
	public VMCell(IValue v) {
		this.v = v;
	}
	
	public IValue get() {
		return v;
	}

	public void set(IValue v) {
		this.v = v;
	}
	
	@Override
	public String toString() {
		return this.v.toString();
	}
}
