package values;

public class VBool implements IValue {
	private boolean bool;
	
	public VBool(boolean bool) {
		this.bool = bool;
	}
	
	public boolean getVal() {
		return bool;
	}
	
	@Override
	public String toString() {
		return "" + bool;
	}
}
