package interpreter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import values.IValue;

public class Environment {
	private LinkedList<Map<String, IValue>> list;

	public Environment() {
		list = new LinkedList<>();
	}

	public Environment beginScope() {
		Map<String, IValue> newMap = new HashMap<>();
		list.push(newMap);
		return this;
	}

	public Environment endScope() {
		list.pop();
		return null;
	}
	
	public void assoc(String id, IValue val) {
		Map<String, IValue> map = list.peekLast();
		map.put(id, val); 
	}
	
	public IValue find(String id) {
		Iterator<Map<String, IValue>> it = list.descendingIterator();
		IValue res = null;
		while (it.hasNext()) {
			Map<String, IValue> map = it.next();
			res = map.get(id);
			if (res != null)
				break;
		}
		if (res == null)
			throw new RuntimeException("Identifier not defined: " + id);
		return res;
	}
}
