package ASTNodes;

import interpreter.Environment;
import values.IValue;

public class ASTPrintln implements ASTNode {
	private ASTNode node = null;

	public ASTPrintln(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment e) {
		IValue res = null;
		if (node != null) {
			res = node.eval(e);
			System.out.println(res.toString());
		} else
			System.out.println();
		return res;
	}
}
