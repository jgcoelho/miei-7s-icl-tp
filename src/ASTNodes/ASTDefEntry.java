package ASTNodes;

public class ASTDefEntry {
	private String id;
	private ASTNode init;

	public ASTDefEntry(String id, ASTNode init) {
		this.id = id;
		this.init = init;
	}

	public String getId() {
		return id;
	}

	public ASTNode getInit() {
		return init;
	}
}