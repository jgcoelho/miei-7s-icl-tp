package ASTNodes;

import java.util.List;

import interpreter.Environment;
import values.IValue;

public class ASTSeq implements ASTNode {
	private List<ASTNode> list;

	public ASTSeq(List<ASTNode> list) {
		this.list = list;
	}

	@Override
	public IValue eval(Environment e) {
		IValue res = null;
		for (ASTNode node : list) {
			res = node.eval(e);
		}
		return res;
	}
}
