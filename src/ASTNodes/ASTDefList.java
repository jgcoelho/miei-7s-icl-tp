package ASTNodes;

import java.util.LinkedList;
import java.util.List;

public class ASTDefList {
	LinkedList<ASTDefEntry> initList;

	public ASTDefList() {
		initList = new LinkedList<>();
	}
	
	public void add(String id, ASTNode init) {
		this.initList.add(new ASTDefEntry(id, init));
	}
	
	public List<ASTDefEntry> getList() {
		return initList;
	}
}
