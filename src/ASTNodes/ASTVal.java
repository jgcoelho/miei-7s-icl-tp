package ASTNodes;
import interpreter.Environment;
import values.IValue;

public class ASTVal implements ASTNode {
	private IValue val;

	public ASTVal(IValue val) {
		this.val = val;
	}

	public IValue eval(Environment e) {
		return val;
	}
}
