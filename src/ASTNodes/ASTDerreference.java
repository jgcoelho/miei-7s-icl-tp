package ASTNodes;

import exception.TypeError;
import interpreter.Environment;
import values.IValue;
import values.VMCell;

public class ASTDerreference implements ASTNode {
	private ASTNode var;
	public ASTDerreference(ASTNode var) {
		this.var = var;
	}

	@Override
	public IValue eval(Environment e) {
		IValue value = var.eval(e);
		if (!(value instanceof VMCell)) {
			throw new TypeError("!: Can't derreference non memory cell");
		}
		return ((VMCell) value).get();
	}
}
