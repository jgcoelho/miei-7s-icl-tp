package ASTNodes;

import interpreter.Environment;
import values.IValue;

public class ASTDef implements ASTNode {
	ASTDefList list;
	private ASTNode body;
	
	public ASTDef(ASTDefList list, ASTNode body) {
		this.list = list;
		this.body = body;
	}

	public IValue eval(Environment e) {
		e.beginScope();
		for (ASTDefEntry entry : this.list.getList())
			e.assoc(entry.getId(), entry.getInit().eval(e));
		IValue bodyVal = body.eval(e);
		e.endScope();
		return bodyVal;
	}
}
