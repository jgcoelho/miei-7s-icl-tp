package ASTNodes;
import interpreter.Environment;
import values.IValue;

public interface ASTNode {
	public IValue eval(Environment e);
}
