package ASTNodes;
import exception.TypeError;
import interpreter.Environment;
import values.IValue;
import values.VInt;

public class ASTMinus implements ASTNode {
	ASTNode node;

	public ASTMinus(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment e) {
		IValue eval = node.eval(e);
		if (eval instanceof VInt) {
			return new VInt(-((VInt) eval).getVal());
		} else {
			throw new TypeError("-: Must be of type Integer");
		}
	}
}
