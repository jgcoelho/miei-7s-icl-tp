package ASTNodes;

import exception.TypeError;
import interpreter.Environment;
import values.IValue;
import values.VBool;

public class ASTWhile implements ASTNode {

	private ASTNode condition;
	private ASTNode instruction;
	
	public ASTWhile(ASTNode condition, ASTNode instruction) {
		this.condition = condition;
		this.instruction = instruction;

	}

	@Override
	public IValue eval(Environment e) {
		IValue conditionValue = condition.eval(e);
		if(!(conditionValue instanceof VBool)) {
			throw new TypeError("while: Not a valid condition, must be boolean");
		}
		IValue result = null;
		while(((VBool)conditionValue).getVal()) {
			result = instruction.eval(e);
			conditionValue = condition.eval(e);
		}
		return result;
	}
}
