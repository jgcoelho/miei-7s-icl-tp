package ASTNodes;

import exception.TypeError;
import interpreter.Environment;
import values.IValue;
import values.VBool;

public class ASTNeg implements ASTNode {
	ASTNode node;

	public ASTNeg(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment e) {
		IValue eval = node.eval(e);
		if (eval instanceof VBool) {
			return new VBool(!((VBool) eval).getVal());
		} else {
			throw new TypeError("~: Argument is not a boolean");
		}
	}
}
