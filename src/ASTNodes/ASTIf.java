package ASTNodes;

import exception.TypeError;
import interpreter.Environment;
import values.IValue;
import values.VBool;

public class ASTIf implements ASTNode{
	
	private ASTNode ifExp; 
	private ASTNode thenExp; 
	private ASTNode elseExp;
	
	public ASTIf(ASTNode ifExp, ASTNode thenExp, ASTNode elseExp) {
		this.ifExp = ifExp;
		this.thenExp = thenExp;
		this.elseExp = elseExp;
	}
	
	@Override
	public IValue eval(Environment e) {
		IValue ifCondition = ifExp.eval(e);
		if(!(ifCondition instanceof VBool)) {
			throw new TypeError("if: Not a valid condition, must be boolean");
		}
		
		if(((VBool)ifCondition).getVal()) {
			return thenExp.eval(e);
		}else {
			return elseExp != null ? elseExp.eval(e): null;
		}
	}
}
