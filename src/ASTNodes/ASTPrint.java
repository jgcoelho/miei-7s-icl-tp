package ASTNodes;

import interpreter.Environment;
import values.IValue;

public class ASTPrint implements ASTNode {
	private ASTNode node;
	
	public ASTPrint(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment e) {
		IValue res = node.eval(e);
		System.out.print(res.toString());
		return res;
	}
}
