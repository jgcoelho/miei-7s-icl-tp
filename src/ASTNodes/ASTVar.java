package ASTNodes;

import interpreter.Environment;
import values.IValue;

public class ASTVar implements ASTNode {
	private String id;

	public ASTVar(String id) {
		this.id = id;
	}

	@Override
	public IValue eval(Environment e) {
		return e.find(id);
	}
}
