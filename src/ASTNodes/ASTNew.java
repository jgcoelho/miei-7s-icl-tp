package ASTNodes;

import interpreter.Environment;
import values.IValue;
import values.VMCell;

public class ASTNew implements ASTNode {
	private ASTNode node;

	public ASTNew(ASTNode node) {
		this.node = node;
	}

	@Override
	public IValue eval(Environment e) {
		IValue value = node.eval(e);
		return new VMCell(value);
	}
}
