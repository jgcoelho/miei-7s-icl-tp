package ASTNodes;

import exception.TypeError;
import interpreter.Environment;
import values.IValue;
import values.VMCell;

public class ASTAssign implements ASTNode {
	private ASTNode n1, n2;

	public ASTAssign(ASTNode n1, ASTNode n2) {
		this.n1 = n1;
		this.n2 = n2;
	}

	@Override
	public IValue eval(Environment e) {
		IValue cell = n1.eval(e);
		if ( !(cell instanceof VMCell) )
			throw new TypeError(":=: Left node must be memory cell");
		IValue value = n2.eval(e);
		((VMCell) cell).set(value);
		return value;
	}
}
