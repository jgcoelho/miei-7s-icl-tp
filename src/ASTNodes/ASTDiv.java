package ASTNodes;
import exception.TypeError;
import interpreter.Environment;
import values.IValue;
import values.VInt;

public class ASTDiv implements ASTNode {
	ASTNode lhs, rhs;

	public ASTDiv(ASTNode l, ASTNode r) {
		lhs = l;
		rhs = r;
	}

	public IValue eval(Environment e) {
		IValue v1 = lhs.eval(e);
		IValue v2 = rhs.eval(e);
		if ((v1 instanceof VInt) && (v2 instanceof VInt)) {
			return new VInt(((VInt)v1).getVal() / ((VInt)v2).getVal());
		} else {
			throw new TypeError("/: Argument is not an integer");
		}
	}
}