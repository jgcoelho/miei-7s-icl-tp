package ASTNodes;

import exception.TypeError;
import interpreter.Environment;
import values.IValue;
import values.VBool;
import values.VInt;

public class ASTLessEq implements ASTNode {
	private ASTNode n1, n2;

	public ASTLessEq(ASTNode n1, ASTNode n2) {
		this.n1 = n1;
		this.n2 = n2;
	}

	@Override
	public IValue eval(Environment e) {
		IValue v1 = n1.eval(e);
		IValue v2 = n2.eval(e);
		if (v1 instanceof VInt && v2 instanceof VInt) {
			return new VBool(((VInt) v1).getVal() <= ((VInt) v2).getVal());
		} else {
			throw new TypeError("<=: Two arguments must be ints");
		}
	}
}
