import java.io.IOException;

import ASTNodes.ASTNode;
import interpreter.Environment;

public class Main {

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws IOException {
		Parser parser = new Parser(System.in);
		ASTNode main;
		
		while (true) {
			try {
				main = parser.Start();
				Main.eval(main);
			} catch (Exception e) {
				System.out.println ("Syntax Error!: " + e.toString());
				e.printStackTrace();
				parser.ReInit(System.in);
			}
		}
	}
	
	public static void eval(ASTNode node) {
		Environment e = new Environment();
		e.beginScope();
		node.eval(e);
		e.endScope();
	}
}
