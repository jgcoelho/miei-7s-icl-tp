package exception;

public class TypeError extends RuntimeException {
	private static final long serialVersionUID = 6380401585838882446L;

	public TypeError(String string) {
		super(string);
	}
}
